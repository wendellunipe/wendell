package pagina;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TesteWendell4 {
	
	@Test
	public void teste4() throws InterruptedException {
		//4� Teste Acessar sistema com os campos v�lidos e validar o email no canto superior direito
		//NOVO DRIVER + LOCAL HTML
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\WENDELL\\Downloads\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://a.testaddressbook.com/");
		
		WebElement singInInicio = driver.findElement(By.id("sign-in"));
		singInInicio.click();
		
		Thread.sleep(3000);
		
		WebElement email = driver.findElement(By.id("session_email"));
		email.clear();
		email.sendKeys("wendellunipe@gmail.com");

		
		WebElement senha = driver.findElement(By.id("session_password"));
		senha.clear();
		senha.sendKeys("123456");
		
		WebElement clickAlertSenha = driver.findElement(By.xpath("//*[@id=\'clearance\']/div/div/form/div[3]/input"));
		clickAlertSenha.click();
		
		WebElement emailValidacaoCantoDireito = driver.findElement(By.xpath("//*[@id=\'navbar\']/div[2]/span"));
		Assert.assertEquals("wendellunipe@gmail.com", emailValidacaoCantoDireito.getText());
		
		
		driver.quit();
		
	}
}
