package pagina;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TesteWendell {
	
	@Test
	public void teste1 () throws InterruptedException {
	
		//1� Teste Acessar sistema com todos os campos vazios e validar a resposta do sistema
			//NOVO DRIVER + LOCAL HTML
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\WENDELL\\Downloads\\chromedriver_win32\\chromedriver.exe");
			WebDriver driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.get("http://a.testaddressbook.com/");
			
			WebElement singInInicio = driver.findElement(By.id("sign-in"));
			singInInicio.click();
			
			Thread.sleep(3000);
			
			WebElement email0 = driver.findElement(By.id("session_email"));
			email0.clear();
			Assert.assertEquals("", email0.getText());
			
			WebElement senha0 = driver.findElement(By.id("session_password"));
			senha0.clear();
			Assert.assertEquals("", senha0.getText());
			
			WebElement singIn = driver.findElement(By.xpath("//*[@id=\'clearance\']/div/div/form/div[3]/input"));
			singIn.click();
			
			WebElement element1 = driver.findElement(By.xpath("//div[@class='alert alert-notice']"));
			Assert.assertEquals("Bad email or password.", element1.getText());
			
			driver.quit();
	
	}
	

}
