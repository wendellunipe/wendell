package pagina;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TesteWendell3 {

	@Test
	public void teste3() throws InterruptedException {
		
		//3� Teste Acessar o sistema com o campo senha vazio e validar a resposta do sistema
			//NOVO DRIVER + LOCAL HTML
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\WENDELL\\Downloads\\chromedriver_win32\\chromedriver.exe");
			WebDriver driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.get("http://a.testaddressbook.com/");
			
			WebElement singInInicio = driver.findElement(By.id("sign-in"));
			singInInicio.click();
			
			Thread.sleep(3000);
			
			WebElement email = driver.findElement(By.id("session_email"));
			email.clear();
			email.sendKeys("wendellunipe@gmail.com");
	
			
			WebElement senha = driver.findElement(By.id("session_password"));
			senha.clear();
			senha.sendKeys("");
			
			WebElement clickAlert = driver.findElement(By.xpath("//*[@id=\'clearance\']/div/div/form/div[3]/input"));
			clickAlert.click();
			
			WebElement element1 = driver.findElement(By.xpath("//div[@class='alert alert-notice']"));
			Assert.assertEquals("Bad email or password.", element1.getText());
			
			driver.quit();
	}
	
}
