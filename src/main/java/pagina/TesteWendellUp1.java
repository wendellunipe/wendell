package pagina;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TesteWendellUp1 {
	@Test
	public void testeUp1() throws InterruptedException {
		
		//1� UP Teste Cadastrar usu�rio com sucesso e verificar a mensagem de sucesso "Welcome to Adress Book"
			//NOVO DRIVER + LOCAL HTML
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\WENDELL\\Downloads\\chromedriver_win32\\chromedriver.exe");
			WebDriver driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.get("http://a.testaddressbook.com/");
		
		WebElement singInInicio = driver.findElement(By.id("sign-in"));
		singInInicio.click();
				Thread.sleep(3000);
			
			//Mudar do SingIn para SingUp
		WebElement singUpTelaSingIN = driver.findElement(By.xpath("//*[@id=\'clearance\']/div/div/form/div[4]/a"));
		singUpTelaSingIN.click();
			//
				Thread.sleep(1000);
		
		WebElement email = driver.findElement(By.xpath("//*[@id=\"user_email\"]"));
		email.clear();
		email.sendKeys("wendellunipe@gmail.com");
		
		WebElement senha = driver.findElement(By.xpath("//*[@id=\"user_password\"]"));
		senha.clear();
		senha.sendKeys("123456");
		
		WebElement email1_2 = driver.findElement(By.xpath("//*[@id=\"user_email\"]"));
		email1_2.clear();
		
		WebElement SingUpTelaSingUp = driver.findElement(By.xpath("//*[@id=\"new_user\"]/div[3]/input"));
		SingUpTelaSingUp.click();
		
		//Mudar do SingUp para SingIn
		WebElement singInTelaSingUp = driver.findElement(By.xpath("//*[@id=\"new_user\"]/div[4]/a"));
		singInTelaSingUp.click();
		//
			Thread.sleep(1000);
		
		WebElement email2 = driver.findElement(By.id("session_email"));
		email2.clear();
		email2.sendKeys("wendellunipe@gmail.com");
		
		WebElement senha2 = driver.findElement(By.id("session_password"));
		senha2.clear();
		senha2.sendKeys("123456");
		
		WebElement singIn = driver.findElement(By.xpath("//*[@id=\"clearance\"]/div/div/form/div[3]/input"));
		singIn.click();

		Thread.sleep(1000);

		WebElement validacaoParaSaberSeChegouNaTelaDeWelcome = driver.findElement(By.xpath("/html/body/div/div/h1"));
		String messageValidate = validacaoParaSaberSeChegouNaTelaDeWelcome.getText();
		Assert.assertEquals("Welcome to Address Book",validacaoParaSaberSeChegouNaTelaDeWelcome.getText());
	
		driver.quit();
	
		
	}
}
